package com.test.user.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.user.api.Application;
import com.test.user.api.dao.UserRepository;
import com.test.user.api.response.UserResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class UserRestControllerIntegrationTest {

    ObjectMapper objectMapper = new ObjectMapper();
    MvcResult mvcResult = null;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void givenRightUser_whenPost_thenStatus200()
            throws Exception {


        MvcResult mvcResult = mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registerSampleRequest()))
                .andExpect(status().isCreated())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.isActive", is("Y")))
                .andReturn();

        removeUser(mvcResult.getResponse().getContentAsString());

    }

    @Test
    public void givenInvalidFormatOnEmail_whenUserPost_thenStatus400()
            throws Exception {

        mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registerSampleBadRequest()))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", is("email field with invalid format")));

    }

    @Test
    public void givenInvalidPassword_whenUserPost_thenStatus400()
            throws Exception {

        mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registerSampleBadRequestWithInvalidPassword()))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", is("password must contain two digits, one uppercase letter and one lowercase letter")));

    }

    @Test
    public void givenRepeatedRequest_whenUserPost_thenStatus400()
            throws Exception {

        MvcResult mvcResult = mvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(registerSampleRequest()))
                .andExpect(status().isCreated())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.isActive", is("Y")))
                .andDo(result -> mvc.perform(post("/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(registerSampleRequest()))
                        .andExpect(status().isBadRequest())
                        .andExpect(content()
                                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                        .andExpect(jsonPath("$.message", is("email already exists"))))
                .andReturn();


        removeUser(mvcResult.getResponse().getContentAsString());
    }

    public void removeUser(String bodyContent) throws IOException {
        UserResponse user = objectMapper.readValue(bodyContent, UserResponse.class);
        userRepository.delete(user.getId());
    }

    private String registerSampleRequest() {
        return "{\n" +
                "    \"name\": \"Juan Rodriguez\",\n" +
                "    \"email\": \"juan@rodriguez.org\",\n" +
                "    \"password\": \"Hunter12\",\n" +
                "    \"phones\": [\n" +
                "        {\n" +
                "            \"number\": \"1234567\",\n" +
                "            \"citycode\": \"1\",\n" +
                "            \"countrycode\": \"57\"\n" +
                "        }\n" +
                "    ]    \n" +
                "}";
    }

    private String registerSampleBadRequest() {
        return "{\n" +
                "    \"name\": \"Juan Rodriguez\",\n" +
                "    \"email\": \"juanrodriguez.org\",\n" + //invalid mail
                "    \"password\": \"Hunter12\",\n" +
                "    \"phones\": [\n" +
                "        {\n" +
                "            \"number\": \"1234567\",\n" +
                "            \"citycode\": \"1\",\n" +
                "            \"countrycode\": \"57\"\n" +
                "        }\n" +
                "    ]    \n" +
                "}";
    }

    private String registerSampleBadRequestWithInvalidPassword() {
        return "{\n" +
                "    \"name\": \"Juan Rodriguez\",\n" +
                "    \"email\": \"juan@rodriguez.org\",\n" + //invalid mail
                "    \"password\": \"hunter\",\n" +
                "    \"phones\": [\n" +
                "        {\n" +
                "            \"number\": \"1234567\",\n" +
                "            \"citycode\": \"1\",\n" +
                "            \"countrycode\": \"57\"\n" +
                "        }\n" +
                "    ]    \n" +
                "}";
    }
}
