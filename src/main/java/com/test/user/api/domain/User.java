package com.test.user.api.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@Entity
@Table(name = "USER_")
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    private String id;
    private String username;
    private String email;
    private String password;
    private Date created;
    private Date modified;
    @Column(name = "LAST_LOGIN")
    private Date lastLogin;
    private String token;
    @Column(name = "IS_ACTIVE")
    private String isActive;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Phone> phones;
}
