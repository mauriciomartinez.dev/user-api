package com.test.user.api.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@Entity
@Table(name = "PHONE_")
@NoArgsConstructor
@AllArgsConstructor
public class Phone {
    @Id
    private String id;
    private Long number;
    private Long cityCode;
    private Long countryCode;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "USER_ID")
    private User user;
}
