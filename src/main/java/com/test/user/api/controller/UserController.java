package com.test.user.api.controller;

import com.test.user.api.request.UserRequest;
import com.test.user.api.response.UserResponse;
import com.test.user.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<UserResponse> register(@Valid @RequestBody UserRequest user) {

        return new ResponseEntity(userService.registerUser(user), HttpStatus.CREATED);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public UserResponse handleUniqueConstraintExceptions(
            DataIntegrityViolationException ex) {
        return UserResponse.builder()
                .message("email already exists")
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public UserResponse handleFieldValidationExceptions(
            MethodArgumentNotValidException ex) {
        return UserResponse.builder()
                .message(ex.getBindingResult()
                        .getAllErrors()
                        .stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(Collectors.joining()))
                .build();
    }

}
