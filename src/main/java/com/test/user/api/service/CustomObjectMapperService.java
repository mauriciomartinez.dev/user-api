package com.test.user.api.service;

import com.test.user.api.domain.User;
import com.test.user.api.request.UserRequest;
import com.test.user.api.response.UserResponse;

public interface CustomObjectMapperService {
    User map(UserRequest userInfo);

    UserResponse map(User user);
}
