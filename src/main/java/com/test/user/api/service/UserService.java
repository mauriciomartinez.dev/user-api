package com.test.user.api.service;

import com.test.user.api.request.UserRequest;
import com.test.user.api.response.UserResponse;

public interface UserService {

    UserResponse registerUser(UserRequest user);
}
