package com.test.user.api.service.impl;

import com.test.user.api.dao.UserRepository;
import com.test.user.api.domain.User;
import com.test.user.api.request.UserRequest;
import com.test.user.api.response.UserResponse;
import com.test.user.api.service.CustomObjectMapperService;
import com.test.user.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomObjectMapperService mapperService;

    @Override
    public UserResponse registerUser(UserRequest userInfo) {

        User user = userRepository.save(mapperService.map(userInfo));
        return mapperService.map(user);
    }
}
