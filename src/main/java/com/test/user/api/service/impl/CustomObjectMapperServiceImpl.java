package com.test.user.api.service.impl;


import com.test.user.api.domain.Phone;
import com.test.user.api.domain.User;
import com.test.user.api.request.UserRequest;
import com.test.user.api.response.UserResponse;
import com.test.user.api.service.CustomObjectMapperService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomObjectMapperServiceImpl implements CustomObjectMapperService {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public User map(UserRequest userInfo) {

        User user = User.builder()
                .id(UUID.randomUUID().toString())
                .username(userInfo.getName())
                .email(userInfo.getEmail())
                .password(passwordEncoder.encode(userInfo.getPassword()))
                .token(getJWTToken(userInfo.getEmail()))
                .created(new Date())
                .modified(new Date())
                .lastLogin(new Date())
                .isActive("Y")
                .build();

        user.setPhones(userInfo.getPhones().stream().map(phone ->
                Phone.builder().
                        id(UUID.randomUUID().toString())
                        .user(user)
                        .number(Long.parseLong(phone.getNumber()))
                        .cityCode(Long.parseLong(phone.getCityCode()))
                        .countryCode(Long.parseLong(phone.getCountryCode()))
                        .build()
        ).collect(Collectors.toList()));
        return user;
    }

    private String getJWTToken(String email) {
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("userJWT")
                .setSubject(email)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(Keys.secretKeyFor(SignatureAlgorithm.HS256)).compact();

        return "Bearer " + token;
    }


    @Override
    public UserResponse map(User user) {
        return UserResponse.builder()
                .id(user.getId())
                .created(formatter.format(user.getCreated()))
                .modified(formatter.format(user.getModified()))
                .isActive(user.getIsActive())
                .lastLogin(formatter.format(user.getLastLogin()))
                .token(user.getToken())
                .build();
    }
}
