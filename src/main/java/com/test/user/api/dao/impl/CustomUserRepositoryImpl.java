package com.test.user.api.dao.impl;

import com.test.user.api.dao.CustomUserRepository;
import com.test.user.api.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CustomUserRepositoryImpl implements CustomUserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User findByEmail(String email) {
        return (User) entityManager.createQuery("FROM User u WHERE u.email = :email")
                .setParameter("email", email)
                .getSingleResult();
    }
}
