package com.test.user.api.dao;

import com.test.user.api.domain.User;

public interface CustomUserRepository {

    User findByEmail(String email);
}
