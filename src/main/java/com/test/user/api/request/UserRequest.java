package com.test.user.api.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest implements Serializable {
    @NotNull(message = "name cannot be missing or empty")
    private String name;
    @Email(message = "email field with invalid format")
    private String email;
    @NotNull(message = "password cannot be missing or empty")
    @Pattern(regexp="^(?=.*[A-Z])(?=.*[a-z])(?=(?:.*[0-9]){2}).*$",message="password must contain two digits, one uppercase letter and one lowercase letter")
    private String password;
    private List<Phone> phones;
}
