DROP TABLE PHONE_ IF EXISTS;
CREATE TABLE PHONE_ (
    ID varchar(255) not null,
    USER_ID varchar(255),
    NUMBER INT not null,
    CITY_CODE INT not null,
    COUNTRY_CODE INT null,
    PRIMARY KEY (ID)
);